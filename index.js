'use strict'

const restify = require('restify')
const server = restify.createServer()


const edamam = require('./food') //variable edamam requires file food
const username = require('./username') //variable username requires file username
const myModule = require('./module') //variable myModule requires file module
const file = require('./storage') //variable file requires storaage

server.use(restify.fullResponse())
server.use(restify.queryParser())
server.use(restify.bodyParser())
server.use(restify.authorizationParser())
//the server will use all of the above functions. for example, bodyParser

file.addfood('banana', (err, result) => { //runs the addfood function from file which in this case we said requires storage at the start of this .js file
	if(err) { // if there is an error, then in the console when running 'node index.js' it will print out error message
		console.log('ERROR')
		console.log(err.message) // console logs message
	} else { // if there is no error then it will continue with the else statement 
		console.log('NO ERROR')
		console.log(result) // there is no error, so we will print the results of searching 
	}
})

server.get('/foods', myModule.dofoodSearch) //./books doBookSearch
server.get('/food', edamam.recipielookup) // do recipelookup
server.get('/favourites', myModule.listFavourites)  // get a list of all favs
server.post('/favourites', myModule.validateFavourite, myModule.addFavourite)  // add a new fav

const port = process.env.PORT || 8080 //runs the code on port 8080. 

server.listen(port, err => console.log(err || `App running on port ${port}`)) //if the code does not run then it will print error